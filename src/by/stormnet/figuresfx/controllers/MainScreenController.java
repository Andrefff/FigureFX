package by.stormnet.figuresfx.controllers;

import by.stormnet.figuresfx.core.figures.Circle;
import by.stormnet.figuresfx.core.figures.Figure;
import by.stormnet.figuresfx.core.figures.Point2D;
import by.stormnet.figuresfx.core.figures.Rectangle;
import by.stormnet.figuresfx.core.interfaces.Drawable;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {
    public Canvas canvas;
    public Button btnClear;
    public Button btnCalc;
    public Button btnExit;

    private List<Drawable> figures;
    private Random random;

    public void onMouseReleased(MouseEvent mouseEvent) {
        System.out.println("canvas clicked");
        System.out.println(String.format("X: %.2f\nY: %.2f", mouseEvent.getX(), mouseEvent.getY()));
        /*Figure figure = createFigure(new Point2D(mouseEvent.getX(), mouseEvent.getY()));
        Drawable drawable = figure;
        figures.add(drawable);*/
        figures.add(createFigure(new Point2D(mouseEvent.getX(), mouseEvent.getY())));
        repaint(canvas.getGraphicsContext2D());
    }

    private void repaint(GraphicsContext graphicsContext) {
        graphicsContext.clearRect(0.0, 0.0, canvas.getWidth(), canvas.getHeight());
        for (Drawable figure : figures) {
            figure.draw(graphicsContext);
        }
    }

    public void clearBtnClicked(MouseEvent mouseEvent) {
        System.out.println("clear");
        canvas.getGraphicsContext2D().clearRect(0.0, 0.0, canvas.getWidth(), canvas.getHeight());
        figures.clear();
    }

    public void calcBtnClicked(MouseEvent mouseEvent) {
        System.out.println("calc");
    }

    public void exitBtnClicked(MouseEvent mouseEvent) {
        Platform.exit();
    }

    private Figure createFigure(Point2D point){
        Figure figure = null;
        switch (getRandomInt(3)){
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(point, getRandomInt(100));
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(point, getRandomInt(100), getRandomInt(100));
                break;
            default:
                System.out.println("Figure type undefined");
                break;
        }

        return figure;
    }

    private int getRandomInt(int bounds){
        int result = random.nextInt(bounds);
        if (result == 0){
            return getRandomInt(bounds);
        }
        return result;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new ArrayList<>();
        random = new Random(System.currentTimeMillis());
    }
}
