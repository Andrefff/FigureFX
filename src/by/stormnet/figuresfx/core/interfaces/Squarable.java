package by.stormnet.figuresfx.core.interfaces;

public interface Squarable {
    double getSquare();
}
