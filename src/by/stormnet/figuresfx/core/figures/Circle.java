package by.stormnet.figuresfx.core.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Circle extends Figure {
    private double radius;

    private Circle(Point2D centerPoint) {
        super(Figure.FIGURE_TYPE_CIRCLE, centerPoint);
    }

    public Circle(Point2D centerPoint, double radius) {
        this(centerPoint);
        if (radius < 10){
            radius = 10;
        }
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext gc) {
        if (gc == null){
            System.out.println("gc == null!!!");
            return;
        }
        gc.setLineWidth(2.0);
        gc.setStroke(Color.DEEPSKYBLUE);
        gc.strokeOval(centerPoint.getX() - radius, centerPoint.getY() - radius, radius * 2, radius * 2);
    }

    @Override
    public double getSquare() {
        return Math.PI * Math.pow(radius, 2);
    }
}
