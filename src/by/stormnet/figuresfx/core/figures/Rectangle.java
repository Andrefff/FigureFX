package by.stormnet.figuresfx.core.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Rectangle extends Figure {
    private double width;
    private double height;
    
    private Rectangle(Point2D centerPoint) {
        super(Figure.FIGURE_TYPE_RECTANGLE, centerPoint);
    }

    public Rectangle(Point2D centerPoint, double width, double height) {
        this(centerPoint);
        if (width < 10){
            width = 10;
        }
        if (height < 10){
            height = 10;
        }
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getheight() {
        return height;
    }

    public void setheight(double height) {
        this.height = height;
    }

    @Override
    public void draw(GraphicsContext gc) {
        if (gc == null) {
            System.out.println("gc == null!!!");
            return;
        }
        gc.setStroke(Color.AQUA);
        gc.setLineWidth(2.0);
        gc.strokeRect(centerPoint.getX() - width / 2, centerPoint.getY() - height / 2, width, height);
    }

    @Override
    public double getSquare() {
        return width * height;
    }
}
