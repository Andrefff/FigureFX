package by.stormnet.figuresfx.core.figures;

import by.stormnet.figuresfx.core.interfaces.Drawable;
import by.stormnet.figuresfx.core.interfaces.Squarable;

public abstract class Figure implements Drawable, Squarable {
    public static final int FIGURE_TYPE_UNDEFINED   = 0;
    public static final int FIGURE_TYPE_CIRCLE      = 1;
    public static final int FIGURE_TYPE_RECTANGLE   = 2;

    protected Point2D centerPoint;
    private int type;

    public Figure(int type, Point2D centerPoint) {
        this.centerPoint = centerPoint;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public Point2D getCenterPoint() {
        return centerPoint;
    }

    public void setCenterPoint(Point2D centerPoint) {
        this.centerPoint = centerPoint;
    }
}
